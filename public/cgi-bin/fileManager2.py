#!/usr/bin/python3

# Copyright (C) 2020 Alex Verrico (https://alexverrico.com/

import cgi
import cgitb
import os

# basedir = r"/var/www/lightweight-journal/"
# journalEntries = r"entries/"
#

print("Content-type: text/html\n")

# webroot = r"/var/www/lightweight-journal/public/"
# generatedEntries = r"entries/"
# templates = r"templates/"

# temp = (basedir, templates, 'entry.html')
# entryTemplate = "".join(temp)
# temp = (basedir, templates, 'home.html')
# homeTemplate = "".join(temp)
# temp = (basedir, templates, 'homeContent.html')
# homeContentTemplate = "".join(temp)

configFile = "config.txt"
varDict = {"basedir": "", "journalentries": "", "webroot": "", "templates": ""}
entryTemplate = ""
homeTemplate = ""
homeContentTemplate = ""
journalEntriesPath = ""
editContentTemplate = ""
fullListContentTemplate = ""


def get_config():
    global varDict
    global entryTemplate
    global homeTemplate
    global homeContentTemplate
    global journalEntriesPath
    global editContentTemplate
    global fullListContentTemplate

    with open(configFile, 'r') as local_f:
        local_filecontents = local_f.readlines()
    for line in local_filecontents:
        line = line.strip()
        var, val = line.split(":")
        varDict[var] = val
    for val in varDict:
        if varDict[val] == "":
            print("Error: variable \"%s\" is undefined!" % val)
            exit(1)
    local_temp = (varDict['basedir'], varDict['templates'], 'entry.html')
    entryTemplate = "".join(local_temp)
    local_temp = (varDict['basedir'], varDict['templates'], 'home.html')
    homeTemplate = "".join(local_temp)
    local_temp = (varDict['basedir'], varDict['templates'], 'homeContent.html')
    homeContentTemplate = "".join(local_temp)
    local_temp = (varDict['basedir'], varDict['templates'], 'editContent.html')
    editContentTemplate = "".join(local_temp)
    local_temp = (varDict['basedir'], varDict['templates'], 'fullListContent.html')
    fullListContentTemplate = "".join(local_temp)
    local_temp = (varDict['basedir'], varDict['journalentries'], '*.md')
    journalEntriesPath = "".join(local_temp)
    return


get_config()

cgitb.enable(logdir=varDict['basedir'])

fs = cgi.FieldStorage()

key = "action"
if key in fs.keys():
    if "file" not in fs.keys() and fs[key].value != "generate":
        print("ERROR, please provide a file name!")
    else:
        if fs[key].value == "load":
            temp = (varDict['basedir'], varDict['journalentries'], fs['file'].value, '.md')
            filepath = "".join(temp)
            if os.path.exists(filepath):
                with open(filepath, 'r') as f:
                    filecontents = f.read()
                    print(filecontents)
            else:
                print("Empty file")
        if fs[key].value == "save":
            if "inputdata" not in fs.keys():
                print("ERROR: Please provide the data to save to the file.<br/>")
            else:
                temp = (varDict['basedir'], varDict['journalentries'], fs['file'].value, '.md')
                filepath = "".join(temp)
                filecontents = fs['inputdata'].value
                if os.path.exists(filepath):
                    os.remove(filepath)
                with open(filepath, 'w') as f:
                    f.write(filecontents)
    if fs[key].value == "generate":
        command = "python3 %sgenerator.py" % varDict['basedir']
        # print(command)
        os.system(command)
