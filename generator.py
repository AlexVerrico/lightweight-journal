# Copyright (C) 2020 Alex Verrico (https://AlexVerrico.com/)

import glob
import os
from markdown import markdown
from pathlib import Path
from datetime import datetime
import shutil

configFile = "config.txt"
varDict = {"basedir": "", "journalentries": "", "webroot": "", "templates": ""}

# basedir = r"/var/www/lightweight-journal/"
# journalEntries = r"entries/"
# webroot = r"/var/www/lightweight-journal/public/"
# generatedEntries = r"entries/"
# templates = r"templates/"

entryTemplate = ""
homeTemplate = ""
homeContentTemplate = ""
journalEntriesPath = ""
editContentTemplate = ""
fullListContentTemplate = ""

# temp = (basedir, templates, 'entry.html')
# entryTemplate = "".join(temp)
# temp = (basedir, templates, 'home.html')
# homeTemplate = "".join(temp)
# temp = (basedir, templates, 'homeContent.html')
# homeContentTemplate = "".join(temp)
# temp = (basedir, journalEntries, '*.md')
# journalEntriesPath = "".join(temp)
# temp = (basedir, templates, 'editContent.html')
# editContentTemplate = "".join(temp)
# temp = (basedir, templates, 'fullListContent.html')
# fullListContentTemplate = "".join(temp)


def get_config():
    global varDict
    global entryTemplate
    global homeTemplate
    global homeContentTemplate
    global journalEntriesPath
    global editContentTemplate
    global fullListContentTemplate

    with open(configFile, 'r') as f:
        filecontents = f.readlines()
    for line in filecontents:
        line = line.strip()
        var, val = line.split(":")
        varDict[var] = val
    for val in varDict:
        if varDict[val] == "":
            print("Error: variable \"%s\" is undefined!" % val)
            exit(1)
    temp = (varDict['basedir'], varDict['templates'], 'entry.html')
    entryTemplate = "".join(temp)
    temp = (varDict['basedir'], varDict['templates'], 'home.html')
    homeTemplate = "".join(temp)
    temp = (varDict['basedir'], varDict['templates'], 'homeContent.html')
    homeContentTemplate = "".join(temp)
    temp = (varDict['basedir'], varDict['templates'], 'editContent.html')
    editContentTemplate = "".join(temp)
    temp = (varDict['basedir'], varDict['templates'], 'fullListContent.html')
    fullListContentTemplate = "".join(temp)
    temp = (varDict['basedir'], varDict['journalentries'], '*.md')
    journalEntriesPath = "".join(temp)
    return


def generate_file_content(name, content, templatelocation):
    content = markdown(content)
    with open(templatelocation, 'r') as f:
        filecontents = f.read()
        generatedcontent = filecontents.replace('{{{title}}}', name)
        generatedcontent = generatedcontent.replace('{{{content}}}', content)
    return generatedcontent


def create_file_list():
    filelist = glob.glob(journalEntriesPath)
    fullfilelist = dict()
    fullnamelist = list()
    for file in filelist:
        with open(file, 'r') as f:
            # first_line = f.readline().strip()
            # date, temp = first_line.split(',')
            filename = Path(file).stem
            filecontents = f.read()
            fullfilelist[filename] = {'contents': filecontents, 'path': file, 'name': filename}
            fullnamelist.append(filename)
    print(fullfilelist)
    fullnamelist.sort(key=lambda date: datetime.strptime(date, "%d-%m-%y"))
    fullnamelist = list(reversed(fullnamelist))
    print(fullnamelist)
    return fullfilelist, fullnamelist


def generate_files(filelist, namelist):
    for name in namelist:
        print(name)
        temp = (varDict['webroot'], 'entries/', filelist[name]['name'], '.html')
        filepath = "".join(temp)
        if os.path.exists(filepath):
            os.remove(filepath)
        with open(filepath, 'w') as f:
            filedata = generate_file_content(filelist[name]['name'], filelist[name]['contents'], entryTemplate)
            f.write(filedata)


# def generate_home(filelist):
#     filelist = list(reversed(filelist))
#     homefilelist = list()
#     homelinks = list()
#     if len(filelist) > 8:
#         x = 8
#     else:
#         x = len(filelist)
#     for i in range(x):
#         homefilelist.append(filelist[i])
#         temp = ('/', generatedEntries, filelist[i]['name'], '.html')
#         filepath = "".join(temp)
#         homelinks.append(filepath)
#     with open(homeContentTemplate, 'r') as f:
#         filecontents = f.read()
#         for i in range(x):
#             filecontents = filecontents.replace('{{{link%s}}}' % i, homelinks[i])
#             filecontents = filecontents.replace('{{{link%stext}}}' % i, homefilelist[i]['name'])
#     homecontent = filecontents
#     with open(homeTemplate, 'r') as f:
#         filecontents = f.read()
#         filecontents = filecontents.replace('{{{title}}}', 'Home | Lightweight Journal')
#         filecontents = filecontents.replace('{{{content}}}', homecontent)
#     temp = (webroot, 'index.html')
#     filepath = "".join(temp)
#     if os.path.exists(filepath) is True:
#         os.remove(filepath)
#     with open(filepath, 'w') as f:
#         f.write(filecontents)


def generate_edit():
    with open(editContentTemplate, 'r') as f:
        filecontents = f.read()
    editcontent = filecontents
    with open(homeTemplate, 'r') as f:
        filecontents = f.read()
        filecontents = filecontents.replace('{{{title}}}', 'Edit | Lightweight Journal')
        filecontents = filecontents.replace('{{{content}}}', editcontent)
    temp = (varDict['webroot'], 'edit.html')
    filepath = "".join(temp)
    if os.path.exists(filepath) is True:
        os.remove(filepath)
    with open(filepath, 'w') as f:
        f.write(filecontents)


def generate_full_list_page(filelist, namelist):
    with open(fullListContentTemplate, 'r') as f:
        filecontents = f.read()
    fulllistcontent = ""
    for name in namelist:
        temp = ('/', 'entries/', filelist[name]['name'], '.html')
        filepath = "".join(temp)
        temp1 = filecontents.replace('{{{link}}}', filepath)
        temp1 = temp1.replace('{{{name}}}', filelist[name]['name'])
        temp = (fulllistcontent, temp1)
        fulllistcontent = "\n".join(temp)
    with open(homeTemplate, 'r') as f:
        filecontents = f.read()
        filecontents = filecontents.replace('{{{title}}}', 'Home | Lightweight Journal')
        filecontents = filecontents.replace('{{{content}}}', fulllistcontent)
    temp = (varDict['webroot'], 'index.html')
    filepath = "".join(temp)
    if os.path.exists(filepath) is True:
        os.remove(filepath)
    with open(filepath, 'w') as f:
        f.write(filecontents)


get_config()
fullFileList, fullNameList = create_file_list()
generate_files(fullFileList, fullNameList)
# generate_home(fullFileList)
generate_edit()
generate_full_list_page(fullFileList, fullNameList)
exit(0)
