import os
import shutil

configFile = "config.txt"
varDict = {"basedir": "", "journalentries": "", "webroot": "", "templates": ""}
entryTemplate = ""
homeTemplate = ""
homeContentTemplate = ""
journalEntriesPath = ""
editContentTemplate = ""
fullListContentTemplate = ""


def get_config():
    global varDict
    global entryTemplate
    global homeTemplate
    global homeContentTemplate
    global journalEntriesPath
    global editContentTemplate
    global fullListContentTemplate

    with open(configFile, 'r') as f:
        filecontents = f.readlines()
    for line in filecontents:
        line = line.strip()
        var, val = line.split(":")
        varDict[var] = val
    for val in varDict:
        if varDict[val] == "":
            print("Error: variable \"%s\" is undefined!" % val)
            exit(1)
    temp = (varDict['basedir'], varDict['templates'], 'entry.html')
    entryTemplate = "".join(temp)
    temp = (varDict['basedir'], varDict['templates'], 'home.html')
    homeTemplate = "".join(temp)
    temp = (varDict['basedir'], varDict['templates'], 'homeContent.html')
    homeContentTemplate = "".join(temp)
    temp = (varDict['basedir'], varDict['templates'], 'editContent.html')
    editContentTemplate = "".join(temp)
    temp = (varDict['basedir'], varDict['templates'], 'fullListContent.html')
    fullListContentTemplate = "".join(temp)
    temp = (varDict['basedir'], varDict['journalentries'], '*.md')
    journalEntriesPath = "".join(temp)
    return


get_config()

temp = (varDict['webroot'], 'cgi-bin/config.txt')
dest = "".join(temp)

if os.path.exists(dest):
    os.remove(dest)
shutil.copy(configFile, dest)
